<?php

namespace App\Exceptions\Message;

use App\Utilities\MessageResponse;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserEmailNotVerified extends MessageResponseException
{
    public function defaultMessageResponse(){
        $response = new MessageResponse();
        $previousUrl = url()->previous();
        $previousRoute = app('router')->getRoutes()->match(app('request')->create($previousUrl))->getName();

        $email = Auth::user()->email;
        if($previousRoute === 'register'){
           $response->title('Your personal data verified successfully!')
                ->subtitle("We've sent you confirmation letter on {$email}");
        }else{
            $subtitle = "We've already sent you confirmation mail on {$email}" ;
            $subtitle .= PHP_EOL . "If you haven't received any letter please click there";
            $response->title('Please,confirm your email!')
                ->subtitle($subtitle);
        }
        return $response->status(403)->get();
    }
}
