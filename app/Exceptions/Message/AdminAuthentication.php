<?php

namespace App\Exceptions\Message;

use App\Utilities\MessageResponse;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;

class AdminAuthentication extends MessageResponseException
{
    public function defaultMessageResponse(){
        return (new MessageResponse())
            ->title("You aren't allowed to visit this page")
            ->subtitle("Please, login as an admin or contact service owner for more information")
            ->status(403)
            ->get();
    }
}
