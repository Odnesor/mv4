<?php

namespace App\Exceptions\Message;

use App\Utilities\MessageResponse;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;

class UserAuthentication extends MessageResponseException
{
    public function defaultMessageResponse(){
        return (new MessageResponse())
            ->title("You aren't authorized")
            ->subtitle("Please, login in order to use our service. You will be redirected in 3 seconds")
            ->redirect(route('login'),3)
            ->status(403)
            ->get();
    }
}
