<?php

namespace App\Exceptions\Message;

use Illuminate\Http\Request;
use Throwable;

abstract class MessageResponseException extends \Exception {

    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->request = $request;
        parent::__construct($message, $code, $previous);
    }

    abstract public function defaultMessageResponse();
}
