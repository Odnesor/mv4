<?php

namespace App\Exceptions\Message;

use App\Utilities\MessageResponse;
use Exception;
use Illuminate\Http\Request;

class AdminNotConfirmed extends MessageResponseException
{
    private $title = 'Service owner has not confirmed your admin account yet';
    private $subtitle = 'Contact owner for more information';


    public function defaultMessageResponse(){
        $previousUrl = url()->previous();
        $previousRoute = app('router')->getRoutes()->match(app('request')->create($previousUrl))->getName();
        if($previousRoute === 'admin.register'){
            $this->setTitle('Your admin registration processed successfully!')
                ->setSubtitle('Please, wait owner\'s confirmation in order to start working with a dashboard!');
        }
        return (new MessageResponse())
            ->title($this->title)
            ->subtitle($this->subtitle)
            ->status(403)
            ->get();
    }

    private function setTitle($title){
        $this->title = $title;
        return $this;
    }

    private function setSubtitle($subtitle){
        $this->subtitle = $subtitle;
        return $this;
    }
}
