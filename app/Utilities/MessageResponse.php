<?php


namespace App\Utilities;


use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;

class MessageResponse
{
    protected string $baseBlade;
    protected string $title;
    protected string $subtitle;
    protected string $redirect;
    protected array $headers;
    protected int $status = 200;
    /**
     * @var ResponseFactory|Response
     */
    private $response;

    public function __construct($blade = 'message'){
        $this->baseBlade = $blade;
    }

    public function get(){
        $response = response()
            ->view($this->baseBlade,[
                'title' => $this->title ?? '',
                'subtitle' => $this->subtitle ?? '',
        ], $this->status);
        if(!empty($this->headers)){
            $response->withHeaders($this->headers);
        }
        return $response;
    }

    public function title(string $title){
        $this->title = $title;
        return $this;
    }

    public function subtitle(string $subtitle){
        $this->subtitle = $subtitle;
        return $this;
    }

    public function status(int $code){
        $this->status = $code;
        return $this;
    }

    public function redirect($url, $time = 5){
        $this->headers['refresh'] = "{$time};{$url}";
        return $this;
    }

    public function redirectBack($time){
        $this->redirect(session()->previousUrl(), $time);
        return $this;
    }
}
