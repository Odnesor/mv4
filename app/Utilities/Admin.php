<?php


namespace App\Utilities;

/**
 * Class Admin
 * @alias Admin
 */
class Admin
{
    /**
     * @param string $redirectUrl
     * @param int $redirectTime
     * @return \Illuminate\Http\Response
     */
    public static function isNotAdminView($redirectUrl = '', $redirectTime = 5){
        $response =  response()->view('auth.admin.message.unauthorized');
        if($redirectUrl){
            $redirectUrl = self::isNotAdminRedirect();
            $response->header("refresh", "{$redirectTime};url={$redirectUrl}");
        }
        return $response;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public static function unconfirmedView(){
        return response()->view('auth.admin.message.unconfirmed');
    }
}
