<?php


namespace App\Repositories;


use App\Models\Role;
use \App\Contracts\RoleRepository as RepositoryContract;
use Illuminate\Support\Collection;

class RoleRepository implements RepositoryContract
{
    public $model;

    public function __construct(Role $role)
    {
        $this->model = $role;
    }

    /**
     * @param string|string[] $slugs
     * @return Role|Collection
     */
    public function getBySlug($slugs)
    {
        if(is_array($slugs)) return $this->model->whereIn('slug', $slugs)->get();
        return $this->model->where('slug',$slugs)->first();
    }

    /**
     * @param string|string[] $slugs
     * @return Role|Collection
     */
    public function getWithSlugExcluded($slugs){
        if(is_array($slugs)) return $this->model->whereNotIn('slug', $slugs)->get();
        return $this->model->where('slug','!=',$slugs)->first();
    }

    public function getAll(){
        return $this->model->all();
    }
}
