<?php


namespace App\Repositories;


use App\Models\User;
use \App\Contracts\UserRepository as Contract;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserRepository implements Contract
{
    private $roleRepository;
    private $model;
    public function __construct(User $user, RoleRepository $roleRepository)
    {
        $this->model = $user;
        $this->roleRepository = $roleRepository;
    }

    public function getByDate()
    {
        return $this->model->whereIn('role_id', $this->getUserRoles()->pluck('id'))
            ->latest()
            ->get();
    }

    public function getUserRoles()
    {
        $adminRolesSlugs = AdminUserRepository::ADMIN_ROLES;
        return $this->roleRepository->getWithSlugExcluded($adminRolesSlugs);
    }

    public function createFromRequest(Request $request)
    {
        $fields = $request->only($this->model::FILLABLE);
        $fields['password'] = Hash::make($request['password']);


        $user =  $this->model->create($fields);
        if(isset($request->role)){
            $role = $this->roleRepository->getBySlug($request->role);
            $user->role()->associate($role)->save();
        }
        return $user;
    }

    public function markUserAsVerified(int $id)
    {
        $this->model->find($id)->update(['email_verified_at' => Carbon::now()]);
    }
}
