<?php


namespace App\Repositories;


use App\Contracts\AdminUsr;
use App\Models\AdminUser;
use App\Models\Role;
use App\Models\User;
use \App\Contracts\UserRepository;
use \App\Contracts\RoleRepository as RoleRepository;
use \App\Contracts\AdminUserRepository as Contract;
use App\Services\Auth\UserRegistrationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminUserRepository implements Contract
{
    const SUPERADMIN_ROLE = 'super_admin';
    const ADMIN_ROLE = 'admin';
    const ADMIN_ROLES = [self::ADMIN_ROLE, self::SUPERADMIN_ROLE];
    private $roleRepository;
    private $userRepository;
    private $model;

    public function __construct(
        AdminUser $admin,
        RoleRepository $roleRepository, UserRepository $userRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->userRepository = $userRepository;
        $this->model = $admin;
    }

    public function getSuperAdminRole()
    {
        return $this->roleRepository->getBySlug(self::SUPERADMIN_ROLE);
    }

    public function getAdminRole()
    {
        return $this->roleRepository->getBySlug(self::ADMIN_ROLE);
    }

    public function getAdminRoles(){
        return $this->roleRepository->getBySlug(self::ADMIN_ROLES);
    }

    public function getSuperAdmin()
    {
        return $this->model->with('relatedUser')
            ->whereHas('relatedUser', function($q){
                $q->where('role_id', $this->getSuperAdminRole()->id);
        })->first();
    }

    public function getAdmins()
    {
        return $this->model->with('relatedUser')
            ->whereHas('relatedUser', function($q){
                $q->whereIn('role_id', $this->getAdminRoles()->pluck('id'));
                $q->where('role_id', '!=', $this->getSuperAdminRole()->id);
            })->get();
    }

    public function createFromRequest(Request $request)
    {
        $relatedUser = $this->userRepository->createFromRequest($request);
        $adminRole = $this->getAdminRole();
        $relatedUser->role()->associate($adminRole)->save();

        $admin = new AdminUser();
        $admin->confirmed_by_owner = false;
        $admin->access_key = Hash::make(time());
        $admin->relatedUser()->associate($relatedUser);
        $admin->save();
        return $admin;
    }

    public function confirm(AdminUser $admin)
    {
        $admin->update(['confirmed_by_owner' =>  1]);
    }
}
