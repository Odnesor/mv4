<?php


namespace App\Repositories;

use App\Contracts\Collection;
use App\Contracts\SubmissionRepository as Contract;
use App\Models\Submission;
use Illuminate\Http\Request;

class SubmissionRepository implements Contract
{
    /**
     * @var Submission
     */
    private $model;

    public function __construct(Submission $submission)
    {
        $this->model = $submission;
    }

    public function getByDate()
    {
        return $this->model->all()->get();
    }

    public function createFromRequest(Request $request)
    {
        return $this->model->create($request->all());
    }
}
