<?php


namespace App\Guards;


use App\Contracts\AdminUserRepository;
use App\Exceptions\Message\AdminNotConfirmed;
use App\Models\AdminUser;
use Illuminate\Auth\GuardHelpers;
use Illuminate\Auth\SessionGuard;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;

class AdminGuard extends SessionGuard
{
    use GuardHelpers;
    protected $request;
    /**
     * @var \App\Repositories\AdminUserRepository
     */
    private $adminUserRepository;

    public function __construct($name, UserProvider $provider, Session $session, \Symfony\Component\HttpFoundation\Request $request = null)
    {
        parent::__construct($name, $provider, $session, $request);
        $this->adminUserRepository = app()->make(AdminUserRepository::class);
    }

    public function login(AuthenticatableContract $user, $remember = false)
    {
        if(!$user instanceof AdminUser) return false;
        return $this->loginAdminThroughRelatedUser($user, $remember);
    }

    public function loginAdminThroughRelatedUser(AdminUser $admin, $remember = false){
        $user = $admin->relatedUser;
        $this->updateSession($admin->getAuthIdentifier());

        // If the user should be permanently "remembered" by the application we will
        // queue a permanent cookie that contains the encrypted copy of the user
        // identifier. We will then decrypt this later to retrieve the users.
        if ($remember) {
            $this->ensureRememberTokenIsSet($user);

            $this->queueRecallerCookie($user);
        }

        // If we have an event dispatcher instance set we will fire an event so that
        // any listeners will hook into the authentication events and run actions
        // based on the login and logout events fired from the guard instances.
        $this->fireLoginEvent($user, $remember);

        $this->setUser($admin);
    }

    public function check()
    {
        if(is_null($this->user())) return false;

        $superAdminUserRole = $this->adminUserRepository->getSuperAdminRole();
        if($this->user()->role == $superAdminUserRole || $this->admin()->confirmed_by_owner == true)
            return $this->user();

        throw new AdminNotConfirmed($this->request);
    }


    public function validate(array $credentials = [])
    {
        if (
            !isset($credentials['email']) ||
            empty($credentials['email']) ||
            !isset($credentials['password']) ||
            empty($credentials['password'])) {
            return false;
        }

        $user = $this->provider->retrieveByCredentials($credentials);

        if (!isset($user)) {
            return false;
        }

        if ($this->provider->validateCredentials($user, $credentials)) {
            $this->setUser($user);
            return true;
        } else {
            return false;
        }
    }

    public function admin(){
        return parent::user();
    }

    public function user(){
        if($admin = $this->admin()) return $admin->relatedUser;
    }
}
