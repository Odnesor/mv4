<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string[]|null  ...$guards
     * @return mixed
     */
    public function handle($request, Closure $next, ...$guards)
    {
        if(Auth::guard('admin')->check()){
            return redirect()->route('admin.manage_users.index');
        }
        if(Auth::guard()->check()){
            return redirect()->route('dashboard');
        }
        return $next($request);
    }
}
