<?php

namespace App\Http\Middleware;

//use App\Utilities\Admin;
use App\Exceptions\Message\AdminAuthentication;
use App\Exceptions\Message\AdminNotConfirmed;
use App\Exceptions\Message\MessageResponseException;
use App\Exceptions\Message\UserAuthentication;
use App\Exceptions\Message\UserEmailNotVerified;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    const GUARD_EXCEPTIONS_MAP = [
      'web' => UserAuthentication::class,
      'admin' => AdminAuthentication::class
    ];
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  Request  $request
     * @return string|null
     */
    public function handle($request, Closure $next, ...$guards)
    {
        try{
            if($guards == ['web']){
                $this->authenticateUser($request);
            }
            if($guards == ['admin']){
                $this->authenticate($request,['admin']);
            }

            return $next($request);
        }catch (MessageResponseException $ex){
            return $ex->defaultMessageResponse();
        }

        $this->authenticate($request, $guards);
        return $next($request);
    }

    private function authenticateUser($request){
        try {
            $this->authenticate($request, ['admin']);
        }catch(AdminAuthentication $ex){
            try{
                $this->authenticate($request, ['web']);
                if(!Auth::user()->hasVerifiedEmail())
                    throw new UserEmailNotVerified($request);
            } catch(\Exception $ex) {throw $ex;}
        }
    }

    protected function unauthenticated($request, array $guards)
    {
        if((count($guards) == 1) &
            array_key_exists($guards[0], self::GUARD_EXCEPTIONS_MAP)
        ){
            $messageResponseExceptionClass = self::GUARD_EXCEPTIONS_MAP[$guards[0]];
            throw new $messageResponseExceptionClass($request);
        }

        throw new AuthenticationException(
            'Unauthenticated.', $guards, $this->redirectTo($request)
        );
    }
}
