<?php

namespace App\Http\Middleware;

use App\Contracts\AdminUserRepository;
use App\Contracts\RoleRepository;
use App\Utilities\MessageResponse;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    private $roleRepository;
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param string[] ...$roles
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$roles)
    {
        if(! in_array(Auth::user()->role->slug, $roles))
            return (new MessageResponse())
                ->title('Forbidden')
                ->subtitle('You should have one of theese roles:'. implode(', ', $roles))
                ->status(403)
                ->get();
        return $next($request);
    }
}
