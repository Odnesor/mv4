<?php


namespace App\Http\View\ShellComposers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;

class AdminSidebarComposer
{
    /**
     * @var array`
     */
    private $menuItems = [];

    public function __construct()
    {
        $this->menuItems = [
            'dashboard' => [
                'url' => route('dashboard'),
                'name' => 'Dashboard',
                'icon' => 'cil-cloud-download',
            ],
            'admin.page.users' => [
                'url' => route('admin.manage_users.index'),
                'name' => 'Users',
                'icon' => 'cil-calculator',
            ]
        ];

        if (Auth::guard()->user()->role == '0') {
            $this->menuItems['admin.admins'] = [
                'url' => route('admin.manage_admins.index'),
                'name' => 'Admins',
                'icon' => 'cil-cloud-download',
            ];
        }
    }

    public function compose(View $view)
    {
        if (isset($this->menuItems[Request::route()->getName()])) {
            $this->menuItems[Request::route()->getName()]['active'] = true;
        }
        $view->with('menuItems', $this->menuItems);
    }
}
