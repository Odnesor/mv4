<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'email' => 'required|unique:users,email|email',
            'first_name' => 'required|min:2',
            'last_name' => 'sometimes|required|min:2',
            'password' => 'required',
            'passwordConfirmation' => 'required_unless:password,null|same:password'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name field is required!',
            'name.min' => 'Name length cant shorter than 3 symbols!',
            'first_name.required' => 'First name is required!',
            'first_name.min' => 'Name length cant shorter than 2 symbols!',
            'last_name.min' => 'Name length cant shorter than 2 symbols!',
            'email.unique' => 'This email has been already signed up!',
            'password.required' => 'Password is required!',
            'passwordConfirmation.required_unless' => 'Confirm your password!',
            'passwordConfirmation.same' => 'Passwords don\'t match!'
        ];
    }


}
