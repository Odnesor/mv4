<?php

namespace App\Http\Controllers\User;

use App\Contracts\RoleRepository;
use App\Contracts\UserRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private $roleRepository;
    private $userRepository;
    public function __construct(RoleRepository $roleRepository, UserRepository $userRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->userRepository = $userRepository;
    }

    public function index(){
        $users = $this->userRepository->getByDate();
        return view('dashboard.users.index', compact('users'));
    }
    public function create(){
        if(Auth::check()) return redirect()->route('dashboard');
        $roles = $this->userRepository->getUserRoles();
        return view('auth.register', compact('roles'));
    }

    public function store(RegisterRequest $request){
        try{
            $user = $this->userRepository->createFromRequest($request);
            if(! Auth::attempt($request->only(['email', 'password']))) throw new \Exception();

            event(new Registered($user));
            return response()->json([
                'success' => true,
                'redirect' => route('dashboard')
            ]);
        }catch (\Exception $ex){
            return response()->json([
                'success' => false,
                'message' => 'Service authorization error. Contact owner for more info',
                'error' => $ex->getMessage()
            ]);
        }
    }

    public function emailVerify(Request $request, int $id){
        $this->userRepository->markUserAsVerified($id);
        return redirect('dashboard');
    }

    public static function passwordRecovery(Request $request){
        return view('auth.password_recovery');
    }
}
