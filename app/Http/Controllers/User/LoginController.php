<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function view(){
        return view('auth.login');
    }

    protected function sendLoginResponse(Request $request)
    {
        return response()->json([
            'success' => true,
            'redirect' => route('dashboard')
        ]);
    }
}
