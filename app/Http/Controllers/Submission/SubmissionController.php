<?php

namespace App\Http\Controllers\Submission;

use App\Contracts\SubmissionRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SubmissionController extends Controller
{
    private $submissionRepository;

    public function __construct(SubmissionRepository $submissionRepository)
    {
        $this->submissionRepository = $submissionRepository;
    }

    public function store(Request $request){
        $submission = $this->submissionRepository->createFromRequest($request);
        dd($submission);
    }
}
