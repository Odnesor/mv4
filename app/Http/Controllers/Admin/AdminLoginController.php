<?php

namespace App\Http\Controllers\Admin;

use App\Guards\AdminGuard;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminLoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers{
        login as protected authTraitLogin;
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function view(){
        return view('auth.admin.login');
    }

    public function login(AdminLoginRequest $request)
    {
        try{
            return $this->authTraitLogin($request);
        }catch(\Exception $ex){
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ]);
        }
    }

    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password', 'access_key');
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        echo 'LOGINFAILED';
        die();
    }

    protected function authenticated(Request $request, $user)
    {
        return response()->json([
           'success' => true,
           'redirect'  => route('admin.manage_users.index')
        ]);
    }

    public function username()
    {
        return 'email';
    }


    /**
     * @return AdminGuard|null
     */
    protected function guard()
    {
        $guard = Auth::guard('admin');
        return $guard instanceof AdminGuard ? $guard : null;
    }
}
