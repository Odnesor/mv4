<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\AdminUserRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminRegisterRequest;
use App\Models\AdminUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminUserController extends Controller
{
    private $adminUserRepository;

    public function __construct(AdminUserRepository $adminUserRepository)
    {
        $this->adminUserRepository = $adminUserRepository;
    }

    public function index()
    {
        $admins = $this->adminUserRepository->getAdmins();
        return view('admin.admins.index', compact('admins'));
    }

    public function create(){
        if(Auth::guard('admin')->check())
            return redirect()->route('admin.manage_users.index');
        return view('auth.admin.register');
    }

    public function store(AdminRegisterRequest $request){
        try{
            $this->adminUserRepository->createFromRequest($request);
            if(! Auth::guard('admin')
                ->attempt([
                    'email' => $request['email'],
                    'password' => $request['password'],
                    'access_key' => 'amp'
                ]))
            {
                throw new \Exception();
            }
            return response()->json([
                'success' => true,
                'redirect' => route('admin.manage_users.index')
            ]);
        }catch (\Exception $ex){
            return response()->json([
                'success' => false,
                'message' => 'Service authorization error. Contact owner for more info'
                ,'error' => $ex->getMessage()
            ]);
        }
    }

    public function confirmRegistrationByOwner(Request $request, AdminUser $admin){
        $this->adminUserRepository->confirm($admin);
        return response()->view('admin.admins.row', compact('admin'));
    }
}
