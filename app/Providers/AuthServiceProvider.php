<?php

namespace App\Providers;

use App\Guards\AdminGuard;
use App\Models\AdminUser;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();


        Auth::provider('admins', function ($app, array $config) {
            return new AdminAuthServiceProvider($app['hash'],AdminUser::class);
        });

// add custom guard
        Auth::extend('admins', function ($app, $name, array $config) {
            $request = $app->make('request');
            $session = $request->session();
            $guard = new AdminGuard(
                $name,
                Auth::createUserProvider( $config['provider'])
                ,$session,
                $request);
//            print_r(get_class(Auth::createUserProvider( $config['provider'])));
//            die();
//            return new AdminGuard(Auth::createUserProvider( $config['provider']), $app->make('request'));
            $guard->setCookieJar($app['cookie']);
            return $guard;
        });
        //
    }
}
