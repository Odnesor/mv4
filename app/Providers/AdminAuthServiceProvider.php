<?php

namespace App\Providers;

use App\Models\AdminUser;
use App\Models\User;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\ServiceProvider;

class AdminAuthServiceProvider extends EloquentUserProvider
{
    public function retrieveByCredentials(array $credentials)
    {
        if($admin = AdminUser::getByUserEmail($credentials['email'])) return $admin;
        throw new \Exception('Admin has not been found');
    }

    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        if(!$user instanceof AdminUser) return false;
        return $this->validateAdminThroughRelatedUser($user, $credentials);
    }

    private function validateAdminThroughRelatedUser(AdminUser $admin, array $credentials){
        $relatedUser = $admin->relatedUser()->first();
        $plain = $credentials['password'];
        $accessKey = $credentials['access_key'];
        if($accessKey !== 'amp') return false;
        if(!$this->hasher->check($plain, $relatedUser->getAuthPassword())) return false;

        return true;
    }

    public function retrieveById($identifier)
    {
        $model = $this->createModel();

        return $this->newModelQuery($model)
            ->where($model->getAuthIdentifierName(), $identifier)
            ->first();
    }

    public function retrieveByToken($identifier, $token)
    {
        $user = User::find($identifier);
        $admin = $user->relatedAdmin;
        if (! $admin) return;

        $rememberToken = $user->getRememberToken();

        return $rememberToken && hash_equals($rememberToken, $token)
            ? $admin : null;
    }
}
