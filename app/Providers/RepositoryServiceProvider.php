<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $repositoriesPath = app_path() . DIRECTORY_SEPARATOR .
            'Repositories' . DIRECTORY_SEPARATOR;
        $contractsPath = app_path() . DIRECTORY_SEPARATOR .
            'Contracts' . DIRECTORY_SEPARATOR;

        foreach(glob("{$repositoriesPath}*.php") as $file){
            $filePath = explode(DIRECTORY_SEPARATOR, $file);
            $class = rtrim($filePath[array_key_last($filePath)], '.php');
            $repositoryInterfacePath = "{$contractsPath}{$class}.php";

            if(!file_exists($repositoryInterfacePath)) continue;
            $this->app->bind("App\\Contracts\\{$class}", "App\\Repositories\\{$class}");
        }
    }
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
