<?php

namespace App\Models;

use App\Http\Requests\RegisterRequest;
use App\Notifications\UserEmailVerification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    const FILLABLE = [
        'role_id','name',
        'first_name', 'last_name',
        'email', 'password', 'email_verified_at'
    ];
    /** @var array */
    protected $fillable = self::FILLABLE;

    /** @var array */
    protected $hidden = [
        'remember_token',
    ];

    /**  @var array */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function relatedAdmin(){
        return $this->hasOne(AdminUser::class);
    }

    public function getRelatedAdmin(){
        return $this->relatedAdmin()->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role(){
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new UserEmailVerification());
    }
}
