<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminUser extends \Illuminate\Foundation\Auth\User
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'confirmed_by_owner'
    ];

    protected $casts = [
      'confirmed_by_owner' => 'boolean'
    ];

    public function relatedUser(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }


    /**
     * @return User
     */
    public function getRelatedUser(){
        return $this->relatedUser()->first();
    }

    public function isSuperAdmin(){

    }


    /**
     * @param string $email
     * @return \Illuminate\Database\Eloquent\Builder|Model|object|null
     */
    public static function getByUserEmail(string $email){
        return self::with('relatedUser')->whereHas('relatedUser', function ($q) use ($email){
            $q->where('email', $email);
        })->first();
    }
}
