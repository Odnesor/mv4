<?php

namespace App\Services\Collections;

use Illuminate\Routing\Route;
use Illuminate\Support\Collection;

class RoutesCollection
{
    public static function boot(){
        Collection::macro('pefixedRoutes', self::prefixedRoutesFilter());
    }


    public static function prefixedRoutesFilter(): \Closure {
        return function(string $prefix){
            return $this->map( function ( Route $route ) use ( $prefix ) {
                $routeName = $route->getName();
                if ( stripos( $routeName, $prefix ) === 0) {
                    return $route;
                }
            } )->filter();
        };
    }
}
