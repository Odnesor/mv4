<?php


namespace App\Services;


use App\Models\Role;

class RoleHelper
{
    public static function getBySlug(string $slug)
    {
        return Role::where(compact('slug'))->firstOrFail();
    }
}

