<?php


namespace App\Contracts;


use App\Models\User;
use Illuminate\Http\Request;

interface UserRepository
{
    public function getByDate();
    public function getUserRoles();

    /**
     * @param Request $request
     * @return User
     */
    public function createFromRequest(Request $request);

    /**
     * @param int $id
     * @return void
     */
    public function markUserAsVerified(int $id);
}
