<?php

namespace App\Contracts;

use App\Models\Submission;
use Illuminate\Http\Request;

interface SubmissionRepository
{
    /**
     * @return Collection
     */
    public function getByDate();

    /**
     * @param Request $request
     * @return Submission
     */
    public function createFromRequest(Request $request);
}
