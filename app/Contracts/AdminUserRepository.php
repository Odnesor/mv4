<?php


namespace App\Contracts;

use App\Models\AdminUser;
use App\Models\Role;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

interface AdminUserRepository
{
    /**
     * @return Role|null
     */
    public function getSuperAdminRole();

    /**
     * @return AdminUser|null
     */
    public function getAdminRole();

    /**
     * @return Collection|null
     */
    public function getAdminRoles();

    /**
     * @return AdminUser|null
     */
    public function getSuperAdmin();

    /**
     * @return Collection|null
     */
    public function getAdmins();

    /**
     * @param Request $request
     * @return AdminUser
     */
    public function createFromRequest(Request $request);

    /**
     * @param AdminUser $admin
     * @return void
     */
    public function confirm(AdminUser $admin);
}
