<?php


namespace App\Contracts;


use App\Models\Role;
use Illuminate\Support\Collection;

interface RoleRepository
{

    /**
     * @param string|string[] $slugs
     * @return Role|Collection
     */
    public function getBySlug($slugs);

    /**
     * @param string[] $slugs
     * @return Role|Collection
     */
    public function getWithSlugExcluded($slugs);
}
