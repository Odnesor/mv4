<?php

namespace Database\Factories;

use App\Models\AdminUser;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class AdminUserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AdminUser::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $accountConfirmationStatus = rand(0,1) == 1;
        return [
            'confirmed_by_owner' => $accountConfirmationStatus,
            'user_id' => 0,
            'access_key' => 'acc'
        ];
    }
}
