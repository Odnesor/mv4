<?php

namespace Database\Seeders;

use App\Contracts\AdminUserRepository;
use App\Models\AdminUser;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AdminUserSeeder extends Seeder
{
    private $adminUserRepository;

    public function __construct(AdminUserRepository $adminUserRepository)
    {
        $this->adminUserRepository = $adminUserRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->assignAdminUser();

        $admins = AdminUser::factory()->times(4)->create()->all();
        array_walk($admins, function(AdminUser $admin){
            $relatedUser = User::factory()->createOne(['role_id' => 2]);
            $admin->relatedUser()->associate($relatedUser);
            $admin->update();
        });
    }

    private function assignAdminUser(): void{
        $superAdminRole = $this->adminUserRepository->getSuperAdminRole();
        $superAdminRelatedUser = User::create([
            'name' => 'Admin name',
            'first_name' => 'AdFirstname',
            'last_name' => 'AdSecondname',
            'email' => 'admin@mail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);
        $superAdmin = AdminUser::create([
            'confirmed_by_owner' => 1,
            'access_key' => 'acc',
        ]);
        $superAdmin->relatedUser()->associate(
            $superAdminRelatedUser->role()->associate($superAdminRole)->save()
        )->save();
    }
}
