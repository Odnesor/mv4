<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    const ROLES = [
        'admin', 'artist', 'manager', 'company'
    ];

    public function run()
    {
        Role::insert([
            [
                'slug' => 'super_admin',
                'name' => 'Super admin',
                'coreui_icon_slug' => ''
            ],
            [
                'slug' => 'admin',
                'name' => 'Admin',
                'coreui_icon_slug' => ''
            ],
            [
                'slug' => 'artist',
                'name' => 'Artist',
                'coreui_icon_slug' => 'cui-music-note'
            ],
            [
                'slug' => 'manager',
                'name' => 'Manager',
                'coreui_icon_slug' => 'cui-briefcase'
            ],
            [
                'slug' => 'company',
                'name' => 'Company',
                'coreui_icon_slug' => 'cui-people'
            ],
        ]);
    }
}
