<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submissions', function (Blueprint $table) {
            $table->id();
//            $table->foreignId('payment_id')->default(0);
//            $table->boolean('confirmed_by_admin')->default(false);

            $table->string('title')->nullable();
            $table->string('artist')->nullable();
            $table->string('streaming_link')->nullable();
            $table->string('description')->nullable();

            $table->string('producer')->nullable();
            $table->string('release_date')->nullable();
            $table->string('featured_artists')->nullable();
            $table->string('record_label')->nullable();

            $table->string('instagram_id')->nullable();
            $table->string('twitter_id')->nullable();
            $table->string('soundcloud_id')->nullable();
            $table->string('spotify_id')->nullable();

            $table->string('special_message')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submissions');
    }
}
