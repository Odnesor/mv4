<tr class="{{$admin->confirmed_by_owner ? 'table-success' : ''}}" >
    <td>{{ $admin->created_at }}</td>
    <td>{{ $admin->relatedUser->name }}</td>
    <td>{{ $admin->relatedUser->email }}</td>
    <td>

            @if($admin->confirmed_by_owner)
                <div class="row align-items-center justify-content-end">
                    <div class="col-6 d-flex justify-content-center">
                        <span class="text-success">confirmed</span>
                    </div>
                    <div class="col-3 d-flex justify-content-center">
                        <form data-disabled="" data-success="table-active" data-error="table-danger">
                            @csrf
                            <button type="submit" class="d-flex btn btn-sm btn-danger">
                                Block
                            </button>
                        </form>
                    </div>
                </div>
            @else
                <div class="d-flex justify-content-center align-items-center">
                    <form method="GET"
                          action="{{route('admin.manage_admins.confirm', ['admin' => $admin->id])}}"
                          data-disabled="table-secondary" data-active="table-success" data-error="table-danger">
                        @csrf

                        <button type="submit" class="btn btn-sm btn-success">
                            Confirm registration
                        </button>
                    </form>
                </div>
            @endif
    </td>
</tr>
