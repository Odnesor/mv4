@extends('dashboard.base')

@section('content')
    <div class="card text-center w-50 m-auto">
        <div class="d-flex justify-content-center card-header">
            <ul class="nav nav-pills card-header-pills w-50 d-flex">
                <li class="nav-item flex-grow-1 justify-content-center">
                    <a class="nav-link active" href="#">
                        <i class="cil-drop1"></i>
                    </a>
                </li>
                <li class="nav-item flex-grow-1 justify-content-center">
                    <a class="nav-link active" href="#">
                        <i class="cil-drop1"></i>
                    </a>
                </li>
                <li class="nav-item flex-grow-1 justify-content-center">
                    <a class="nav-link" href="#">
                        <i class="cil-drop1"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <h5 class="card-title">Special title treatment</h5>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>

            <form method="POST" action="{{ route('submission') }}" id="registerForm">

                @csrf
{{--                <div class="d-flex flex-column align-items-center mb-3">--}}
{{--                    <div class="role-selector">--}}
{{--                        <h1>{{ __('Register') }} <span class="text-muted">as a</span></h1>--}}
{{--                        <div class="nav d-flex btn-group btn-group-toggle"--}}
{{--                             role="tablist">--}}
{{--                            @foreach($roles as $role)--}}
{{--                                <label class="btn btn-square btn-outline-secondary @if($loop->first) active @endif"--}}
{{--                                       data-toggle="tab" href="#{{$role['slug']}}" role="tab">--}}
{{--                                    <input type="radio" name="role" autocomplete="off" value="{{$role['slug']}}"> {{ucfirst($role['name'])}}--}}
{{--                                </label>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}


{{--                <div class="tab-content row justify-content-center mb-5">--}}
{{--                    @foreach($roles as $role)--}}
{{--                        <div class="col-8 tab-pane @if($loop->first) active @endif " role="tabpanel" id="{{$role['slug']}}">--}}
{{--                            <div class="input-group">--}}
{{--                                @include('auth.input.text', ['name' => 'name',--}}
{{--                                                            'placeholder' => ucfirst($role['name']) . ' name',--}}
{{--                                                            'icon' => $role['coreui_icon_slug']])--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    @endforeach--}}
{{--                </div>--}}

                <div class="row">
                    <div class="col-md-6 mb-3">
                        @include('auth.input.text', ['name' => 'title',
                                                    'placeholder' => 'title',
                                                    'icon' => 'cui-user'])
                    </div>
                    <div class="col-md-6 mb-3">
                        @include('auth.input.text', ['name' => 'artist',
                                                    'placeholder' => 'artist',
                                                    'icon' => 'cui-user'])
                    </div>
                    <div class="col-md-12 mb-3">
                        @include('auth.input.text', ['name' => 'streaming_link',
                                                    'placeholder' => 'streaming_link',
                                                    'icon' => 'cui-streaming_link'])
                    </div>
                    <div class="col-md-12 mb-3">
                        @include('auth.input.text', ['name' => 'description',
                                                    'placeholder' => 'description',
                                                    'icon' => 'cui-description'])
                    </div>



                    <div class="col-md-12 mb-5">
                        @include('auth.input.text', ['name' => 'producer',
                                                    'placeholder' => 'producer',
                                                    'icon' => 'cui-description'])
                    </div>
                    <div class="col-md-12 mb-3">
                        @include('auth.input.text', ['name' => 'release_date',
                                                    'placeholder' => 'release_date',
                                                    'icon' => 'cui-description'])
                    </div>
                    <div class="col-md-12 mb-3">
                        @include('auth.input.text', ['name' => 'featured_artists',
                                                    'placeholder' => 'featured_artists',
                                                    'icon' => 'cui-description'])
                    </div>
                    <div class="col-md-12 mb-3">
                        @include('auth.input.text', ['name' => 'record_label',
                                                    'placeholder' => 'record_label',
                                                    'icon' => 'cui-description'])
                    </div>
                    <div class="col-md-12 mb-3">
                        @include('auth.input.text', ['name' => 'record_label',
                                                    'placeholder' => 'record_label',
                                                    'icon' => 'cui-description'])
                    </div>

                    <div class="col-md-6 mb-3">
                        @include('auth.input.text', ['name' => 'instagram_id',
                                                    'placeholder' => 'instagram_id',
                                                    'icon' => 'cui-description'])
                    </div>
                    <div class="col-md-6 mb-3">
                        @include('auth.input.text', ['name' => 'twitter_id',
                                                    'placeholder' => 'twitter_id',
                                                    'icon' => 'cui-description'])
                    </div>
                    <div class="col-md-6 mb-3">
                        @include('auth.input.text', ['name' => 'soundcloud_id',
                                                    'placeholder' => 'soundcloud_id',
                                                    'icon' => 'cui-description'])
                    </div>
                    <div class="col-md-6 mb-3">
                        @include('auth.input.text', ['name' => 'spotify_id',
                                                    'placeholder' => 'spotify_id',
                                                    'icon' => 'cui-description'])
                    </div>
                </div>

                <button id="registerSubmit" class="btn btn-block btn-danger" type="submit">{{ __('Register') }}</button>
            </form>
            <a href="#" class="btn btn-primary">Next</a>
        </div>
    </div>
@endsection
