<div class="c-sidebar-brand">
    <img class="c-sidebar-brand-full" src="http://localhost/assets/brand/coreui-base-white.svg" width="118" height="46" alt="CoreUI Logo">
    <img class="c-sidebar-brand-minimized" src="http://localhost/assets/brand/coreui-signet-white.svg" width="118" height="46" alt="CoreUI Logo">
</div>

<ul class="c-sidebar-nav ps ps--active-y">
    @foreach($menuItems as $item)
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link @isset($item['active'])c-active @endisset "
               href="{{$item['url']}}">
                <i class="{{$item['icon']}} c-sidebar-nav-icon"></i>
                {{$item['name']}}
            </a>
        </li>
    @endforeach

    <li class="c-sidebar-nav-dropdown"><a class="c-sidebar-nav-dropdown-toggle" href="#"><i class="cil-calculator c-sidebar-nav-icon"></i>Settings</a><ul class="c-sidebar-nav-dropdown-items"><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/notes"><span class="c-sidebar-nav-icon"></span>Notes</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/users"><span class="c-sidebar-nav-icon"></span>Users</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/menu/menu"><span class="c-sidebar-nav-icon"></span>Edit menu</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/menu/element"><span class="c-sidebar-nav-icon"></span>Edit menu elements</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/roles"><span class="c-sidebar-nav-icon"></span>Edit roles</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/media"><span class="c-sidebar-nav-icon"></span>Media</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/bread"><span class="c-sidebar-nav-icon"></span>BREAD</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/mail"><span class="c-sidebar-nav-icon"></span>Email</a></li></ul></li>                                                                <li class="c-sidebar-nav-title">
        Theme
    </li>
    <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="http://localhost/colors">
            <i class="cil-drop1 c-sidebar-nav-icon"></i>
            Colors
        </a>
    </li>

    <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="http://localhost/typography">
            <i class="cil-pencil c-sidebar-nav-icon"></i>
            Typography
        </a>
    </li>
    <li class="c-sidebar-nav-dropdown"><a class="c-sidebar-nav-dropdown-toggle" href="#"><i class="cil-puzzle c-sidebar-nav-icon"></i>Base</a><ul class="c-sidebar-nav-dropdown-items"><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/breadcrumb"><span class="c-sidebar-nav-icon"></span>Breadcrumb</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/cards"><span class="c-sidebar-nav-icon"></span>Cards</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/carousel"><span class="c-sidebar-nav-icon"></span>Carousel</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/collapse"><span class="c-sidebar-nav-icon"></span>Collapse</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/forms"><span class="c-sidebar-nav-icon"></span>Forms</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/jumbotron"><span class="c-sidebar-nav-icon"></span>Jumbotron</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/list-group"><span class="c-sidebar-nav-icon"></span>List group</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/navs"><span class="c-sidebar-nav-icon"></span>Navs</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/pagination"><span class="c-sidebar-nav-icon"></span>Pagination</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/popovers"><span class="c-sidebar-nav-icon"></span>Popovers</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/progress"><span class="c-sidebar-nav-icon"></span>Progress</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/scrollspy"><span class="c-sidebar-nav-icon"></span>Scrollspy</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/switches"><span class="c-sidebar-nav-icon"></span>Switches</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/tables"><span class="c-sidebar-nav-icon"></span>Tables</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/tabs"><span class="c-sidebar-nav-icon"></span>Tabs</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/base/tooltips"><span class="c-sidebar-nav-icon"></span>Tooltips</a></li></ul></li>                                                                <li class="c-sidebar-nav-dropdown"><a class="c-sidebar-nav-dropdown-toggle" href="#"><i class="cil-cursor c-sidebar-nav-icon"></i>Buttons</a><ul class="c-sidebar-nav-dropdown-items"><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/buttons/buttons"><span class="c-sidebar-nav-icon"></span>Buttons</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/buttons/button-group"><span class="c-sidebar-nav-icon"></span>Buttons Group</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/buttons/dropdowns"><span class="c-sidebar-nav-icon"></span>Dropdowns</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/buttons/brand-buttons"><span class="c-sidebar-nav-icon"></span>Brand Buttons</a></li></ul></li>                                                                <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="http://localhost/charts">
            <i class="cil-chart-pie c-sidebar-nav-icon"></i>
            Charts
        </a>
    </li>
    <li class="c-sidebar-nav-dropdown"><a class="c-sidebar-nav-dropdown-toggle" href="#"><i class="cil-star c-sidebar-nav-icon"></i>Icons</a><ul class="c-sidebar-nav-dropdown-items"><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/icon/coreui-icons"><span class="c-sidebar-nav-icon"></span>CoreUI Icons</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/icon/flags"><span class="c-sidebar-nav-icon"></span>Flags</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/icon/brands"><span class="c-sidebar-nav-icon"></span>Brands</a></li></ul></li>                                                                <li class="c-sidebar-nav-dropdown"><a class="c-sidebar-nav-dropdown-toggle" href="#"><i class="cil-bell c-sidebar-nav-icon"></i>Notifications</a><ul class="c-sidebar-nav-dropdown-items"><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/notifications/alerts"><span class="c-sidebar-nav-icon"></span>Alerts</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/notifications/badge"><span class="c-sidebar-nav-icon"></span>Badge</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/notifications/modals"><span class="c-sidebar-nav-icon"></span>Modals</a></li></ul></li>                                                                <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link c-active" href="http://localhost/widgets">
            <i class="cil-calculator c-sidebar-nav-icon"></i>
            Widgets
        </a>
    </li>
    <li class="c-sidebar-nav-title">
        Extras
    </li>
    <li class="c-sidebar-nav-dropdown"><a class="c-sidebar-nav-dropdown-toggle" href="#"><i class="cil-star c-sidebar-nav-icon"></i>Pages</a><ul class="c-sidebar-nav-dropdown-items"><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/login"><span class="c-sidebar-nav-icon"></span>Login</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/register"><span class="c-sidebar-nav-icon"></span>Register</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/404"><span class="c-sidebar-nav-icon"></span>Error 404</a></li><li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="http://localhost/500"><span class="c-sidebar-nav-icon"></span>Error 500</a></li></ul></li>                                                                <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="https://coreui.io">
            <i class="cil-cloud-download c-sidebar-nav-icon"></i>
            Download CoreUI
        </a>
    </li>
    <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="https://coreui.io/pro/">
            <i class="cil-layers c-sidebar-nav-icon"></i>
            Try CoreUI PRO
        </a>
    </li>
    <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="https://coreui.io/pro/">
            <i class="cil-layers c-sidebar-nav-icon"></i>
            Try CoreUI PRO
        </a>
    </li>
    <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 724px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 711px;"></div></div></ul>
<button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
