<!DOCTYPE html>
<!--
* CoreUI Free Laravel Bootstrap Admin Template
* @version v2.0.1
* @link https://coreui.io
* Copyright (c) 2020 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>CoreUI Free Bootstrap Admin Template</title>
    <meta name="theme-color" content="#ffffff">
    <!-- Icons-->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics-->

</head>
<body>

<div class="min-vh-100 d-flex flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="float-start col-md-6">
                <h1>
                    @isset($title) {{$title}}
                    @else @yield('title')
                    @endisset
                </h1>
                <p class="text-medium-emphasis">
                    @isset($subtitle) {{$subtitle}}
                    @else @yield('subtitle')
                    @endisset
                </p>
            </div>
        </div>
    </div>
</div>

<!-- CoreUI and necessary plugins-->
<script src="https://code.jquery.com/jquery-3.6.0.min.js" crossorigin="anonymous"></script>
<script src="{{ asset('js/coreui.bundle.js') }}"></script>
@yield('javascript')

</body>
</html>
