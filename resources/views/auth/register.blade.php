@extends('auth.base')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card mx-4">
                    <div class="card-body p-4">
                        <form method="POST" action="{{ route('register') }}" id="registerForm">
                            @csrf
                            <div class="d-flex flex-column align-items-center mb-3">
                                <div class="role-selector">
                                    <h1>{{ __('Register') }} <span class="text-muted">as a</span></h1>
                                    <div class="nav d-flex btn-group btn-group-toggle"
                                        role="tablist">
                                        @foreach($roles as $role)
                                            <label class="btn btn-square btn-outline-secondary @if($loop->first) active @endif"
                                                   data-toggle="tab" href="#{{$role['slug']}}" role="tab">
                                                <input type="radio" name="role" autocomplete="off" value="{{$role['slug']}}"> {{ucfirst($role['name'])}}
                                            </label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>


                            <div class="tab-content row justify-content-center mb-5">
                                @foreach($roles as $role)
                                    <div class="col-8 tab-pane @if($loop->first) active @endif " role="tabpanel" id="{{$role['slug']}}">
                                        <div class="input-group">
                                            @include('auth.input.text', ['name' => 'name',
                                                                        'placeholder' => ucfirst($role['name']) . ' name',
                                                                        'icon' => $role['coreui_icon_slug']])
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    @include('auth.input.text', ['name' => 'first_name',
                                                                'placeholder' => 'First name',
                                                                'icon' => 'cui-user'])
                                </div>
                                <div class="col-md-6 mb-3">
                                    @include('auth.input.text', ['name' => 'last_name',
                                                                'placeholder' => 'Last name',
                                                                'icon' => 'cui-user'])
                                </div>
                                <div class="col-12 mb-3">
                                    @include('auth.input.text', ['name' => 'email',
                                                                'type' => 'email',
                                                                'placeholder' => 'Email',
                                                                'icon' => 'cui-envelope-closed'])
                                </div>
                                <div class="col-md-6 mb-3">
                                    @include('auth.input.text', ['name' => 'password',
                                                                'type' => 'password',
                                                                'placeholder' => 'Password',
                                                                'icon' => 'cui-applications'])
                                </div>
                                <div class="col-md-6 mb-3">
                                    @include('auth.input.text', ['name' => 'passwordConfirmation',
                                                                'type' => 'password',
                                                                'placeholder' => 'Repeat password',
                                                                'icon' => 'cui-applications'])</div>
                            </div>

                            <button id="registerSubmit" class="btn btn-block btn-danger" type="submit">{{ __('Register') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
