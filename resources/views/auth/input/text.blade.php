<div class="input-group">
    <div class="input-group-prepend">
    <span class="input-group-text">
    <svg class="c-icon">
        <use xlink:href="{{asset("assets/icons/coreui/free-symbol-defs.svg#{$icon}")}}"></use>
    </svg>
    </span>
    </div>
    <input class="form-control"
           type="{{$type ?? 'text'}}"
           placeholder="{{$placeholder}}"
           name="{{$name}}"
           autofocus>
</div>
