@extends('auth.base')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card mx-4">
                    <div class="card-body p-4">
                        <form method="POST" action="{{ route('admin.register') }}" id="adminRegisterForm">
                            @csrf
                            <h1 class="mb-3">{{ __('Register') }} <span class="text-muted">as an admin</span></h1>

                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    @include('auth.input.text', ['name' => 'first_name',
                                                                'placeholder' => 'First name',
                                                                'icon' => 'cui-user'])
                                </div>
                                <div class="col-md-6 mb-3">
                                    @include('auth.input.text', ['name' => 'last_name',
                                                                'placeholder' => 'Last name',
                                                                'icon' => 'cui-user'])
                                </div>
                                <div class="col-12 mb-3">
                                    @include('auth.input.text', ['name' => 'email',
                                                                'type' => 'email',
                                                                'placeholder' => 'Email',
                                                                'icon' => 'cui-envelope-closed'])
                                </div>
                                <div class="col-md-6 mb-3">
                                    @include('auth.input.text', ['name' => 'password',
                                                                'type' => 'password',
                                                                'placeholder' => 'Password',
                                                                'icon' => 'cui-applications'])
                                </div>
                                <div class="col-md-6 mb-3">
                                    @include('auth.input.text', ['name' => 'passwordConfirmation',
                                                                'type' => 'password',
                                                                'placeholder' => 'Repeat password',
                                                                'icon' => 'cui-applications'])</div>
                            </div>

                            <button id="registerSubmit" class="btn btn-block btn-danger" type="submit">{{ __('Register') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
