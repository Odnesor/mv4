function asyncRow(row, form){
    let disabledColor = form.data('disabled');
    let activeColor = form.data('active');
    let errorColor = form.data('error');

    var data = new FormData(form.get(0));

    let rememberClass = row.attr('class')
    row.addClass(disabledColor);
    var spinner =  document.createElement('div');
    spinner.classList.add('spinner-border', 'text-secondary');
    form.find('button').replaceWith(spinner)
    row.removeClass(`${errorColor} ${activeColor}`, { duration:1000, children:true })
    $.ajax({
        url: form.attr('action'),
        processData: false,
        data: data,
        method: form.attr('method'),
        success: function (data){
            row.replaceWith(data)
        }
    })
}


$(document).on('submit', 'table.async-rows tr form', function (event){
    event.preventDefault();
    var form = $(this);
    var row = $(this).closest('tr');
    asyncRow(row, form);
})
