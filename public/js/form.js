function validate(form, formDataExecuteFunction){
    let $form = form;
    var $formSubmit = $form.find('button[type="submit"]')
    $form.on('submit', function(event){
        event.preventDefault()
        if($formSubmit.attr('disabled') !== undefined) return;

        formReload($form);
        $formSubmit.attr('disabled', 'disabled').button('refresh')
        $.ajax({
            url: $form.attr('action'),
            data: formDataExecuteFunction(form),
            method: $form.attr('method'),
            error: function(data){
                var errors = $.parseJSON(data.responseText).errors;
                formDisplayErrors(errors, $form)
                $formSubmit.removeAttr('disabled').button('refresh')
            },
            success: function (data){
                $formSubmit.removeAttr('disabled').button('refresh')
                if(!data.success) return;
                window.location.href = data.redirect
            }
        })
    })
}

function formDisplayErrors(errorsList, form){
    $.each(errorsList, function(inputName, errorMessage){
        var appropriateInput = form.find(`input[name="${inputName}"]`)
        var messageElement = $('<div>', {'class': 'invalid-feedback'})
        messageElement.text(errorMessage)
        appropriateInput.addClass('is-invalid')
        messageElement.insertAfter(appropriateInput)
    })
}

function formReload(form){
    form.find('input').each(function(count, input){
        input = $(input)
        input.removeClass('is-invalid')
        input.parent().children('div.invalid-feedback').remove()
    })
}

function getRegisterFormData(form){
    let activeNameInput = form.find('.tab-pane.active input[name="name"]');

    return {
        name: activeNameInput.val(),
        role: form.find('.role-selector .active input').val(),
        email: form.find('input[name="email"]').val(),
        first_name: form.find('input[name="first_name"]').val(),
        last_name: form.find('input[name="last_name"]').val(),
        password: form.find('input[name="password"]').val(),
        passwordConfirmation: form.find('input[name="passwordConfirmation"]').val(),
        _token: form.find('input[name="_token"').val()
    }
}

function getAdminRegisterFormData(form){
    return {
        email: form.find('input[name="email"]').val(),
        first_name: form.find('input[name="first_name"]').val(),
        last_name: form.find('input[name="last_name"]').val(),
        password: form.find('input[name="password"]').val(),
        passwordConfirmation: form.find('input[name="passwordConfirmation"]').val(),
        _token: form.find('input[name="_token"').val()
    }
}

function getAdminLoginFormData(form) {
    return {
        email: form.find('input[name="email"]').val(),
        password: form.find('input[name="password"]').val(),
        access_key: form.find('input[name="access_key"]').val(),
        _token: form.find('input[name="_token"').val()
    }
}

function getLoginFormData(form){
    return {
        email: form.find('input[name="email"]').val(),
        password: form.find('input[name="password"]').val(),
        _token: form.find('input[name="_token"').val()
    }
}

$(document).ready(function(){
    let $registerForm = $('#registerForm')
    let $loginForm = $('#loginForm')
    let $adminLoginForm = $('#adminLoginForm');
    let $adminRegisterForm = $('#adminRegisterForm');
    validate($registerForm, getRegisterFormData);
    validate($loginForm, getLoginFormData);
    validate($adminLoginForm, getAdminLoginFormData);
    validate($adminRegisterForm, getAdminRegisterFormData);
})
