<?php

use App\Http\Controllers\Admin\AdminLoginController;
use App\Http\Controllers\Admin\AdminUserController;
use App\Http\Controllers\User\LoginController;
use App\Http\Controllers\User\UserController;
use App\Models\AdminUser;
use App\Models\Role;
use App\Models\User;
use App\Repositories\AdminUserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/login', [LoginController::class, 'view'])->name('login');
Route::post('login', [LoginController::class, 'login'])->name('login');
Route::get('/register', [UserController::class, 'create'])->name('register');
Route::post('/register', [UserController::class, 'store'])->name('register');

Route::name('admin.')->prefix('admin')->group(function () {
    Route::get('login', [AdminLoginController::class, 'view'])->name('login');
    Route::post('login', [AdminLoginController::class, 'login'])->name('login');
    Route::get('register', [AdminUserController::class, 'create'])->name('register');
    Route::post('register', [AdminUserController::class, 'store'])->name('register');
});
Route::post('logout', [LoginController::class, 'logout'])->name('logout');
Route::get('/email/verify/{id}', [UserController::class, 'emailVerify'])->name('email.verify');
Route::get('/password-recovery', [UserController::class, 'passwordRecovery'])->name('password.recovery');

Route::middleware('auth:web')->group(function () {
    Route::view('/dashboard', 'dashboard.base')->name('dashboard');
    Route::redirect('/','/dashboard');

    Route::middleware('auth:admin')->name('admin.')->prefix('admin')
        ->group(function () {
            Route::name('manage_users.')->group(function () {
                Route::get('/users', [UserController::class, 'index'])->name('index');
            });
            Route::middleware('role:' . AdminUserRepository::SUPERADMIN_ROLE)->group(function () {
                Route::name('manage_admins.')->group(function(){
                    Route::get('/manage-admins', [AdminUserController::class, 'index'])->name('index');
                    Route::get('/admin/confirm/{admin}', [AdminUserController::class, 'confirmRegistrationByOwner'])->name('confirm');
                });
            });
        });
});

Route::get('/drop', function () {
    foreach(AdminUser::all() as $admin){
        $admin->confirmed_by_owner = false;
        $admin->update();
    }
    echo 'dropped';
});

Route::get('/users', function () {
    ?>
    <pre>
        <?php
        print_r(User::all()->toArray())
        ?>
    </pre>
    <?php
});

Route::get('/admins', function () {
    ?>
    <pre>
        <?php
        print_r(AdminUser::all()->toArray());
        print_r(AdminUser::getByUserEmail('cecelia.murray@example.net')->access_key);
        ?>
    </pre>
    <?php
});

Route::get('/test', function () {
    dd(Role::getUserRolesArray());
});
