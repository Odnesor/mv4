function restart() {
    source .dev.sh
}

function view(){
    IFS='/'
    read -a path <<< $1
    dir="resources/views"
    for (( i=0; i<${#path[@]}-1; i++))
    do
        dir="${dir}/${path[$i]}"
    done
    file=${path[${#path[@]} - 1]}
    mkdir -p "$dir"
    touch "${dir}/${file}.blade.php"
}

function pam(){
    php artisan make:$@
}
